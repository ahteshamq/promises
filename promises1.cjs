const fs = require("fs")
const path = require("path")

function promise1() {

    //let dirPath = path.join(__dirname, "/tst/randomJson")             //test: failed folder creation
    let dirPath = path.join(__dirname, "/test/randomJson")

    createDir(dirPath)
        .then(function (status) {
            console.log(status)
            return createJsonFiles(dirPath)
        })
        .then(function (arrFiles) {
            return deleteJsonFiles(dirPath, arrFiles)
            //return deleteJsonFiles(dirPath, [])           // test: no files to delete
        }).then(function (message) {
            console.log(message)
        })
        .catch(function (err) {
            console.error(err)
        })
}

function createDir(dirPath) {
    const promise = new Promise(function (resolve, rejects) {
        fs.mkdir(dirPath, function (err) {
            if (err) {
                rejects(new Error(JSON.stringify({ "Could not create folder": err })))
            } else {
                resolve("Json folder created successfully.")
            }
        })
    })

    return promise
}

function deleteJsonFiles(dirPath, arrfiles) {
    const promise = new Promise(function (resolve, rejects) {
        if (arrfiles.length === 0) {
            rejects("No files to delete.")
        } else {
            let counter = 0
            function recursiveDelete(count) {
                if (count === arrfiles.length) {
                    resolve("All files deleted successfully")
                } else {
                    /*
                    if (count === 4) {
                        count = 0
                    }*/                //test: some files not deleted.
                    let filepath = path.join(dirPath, `jsonFile${count + 1}`)
                    fs.unlink(filepath, function (err) {
                        if (err) {
                            rejects(new Error(JSON.stringify({ "Error while deleting files": err })))
                        } else {
                            console.log(`jsonFile${count + 1} deleted successfully.`)
                            recursiveDelete(count + 1)
                        }
                    })
                }
            }
            recursiveDelete(counter)
        }
    })
    return promise
}

function createJsonFiles(dirPath) {
    const promise = new Promise(function (resolve, rejects) {
        console.log("File creation starting.");
        let randomInt = Math.floor(Math.random() * (10 - 1 + 1) + 1); // 10 is max and 1 is min limit for random number.
        let filenames = [];
        let count = 0
        function recursive(count) {
            if (randomInt === count) {
                console.log("All files created successfully.")
                resolve(filenames)
            } else {
                let filename = `jsonFile${count + 1}`
                let fileData = {
                    [filename]: `This is json file ${count + 1}`
                }
                /*
                if (count === 4) {
                    dirPath = "sdksm"
                }*/                         //test: Error while file creation.

                fs.writeFile(path.join(dirPath, filename), JSON.stringify(fileData), function (err) {
                    if (err) {
                        rejects(new Error(JSON.stringify({ "Error while file creation": err })))
                    } else {
                        console.log(`${filename} created successfully.`)
                        filenames.push(filename)
                        recursive(count + 1)
                    }
                })
            }
        }
        recursive(count)
    })
    return promise
}


module.exports = promise1;