const path = require("path")
const promise2 = require("../promises2.cjs")

let limpsumPath = path.join(__dirname, "lipsum.txt")

//promise2("/a/")             //test: Invalid lipsum content path
promise2(limpsumPath)