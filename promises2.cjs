const { rejects } = require("assert")
const fs = require("fs")
const { resolve } = require("path")
const path = require("path")


function promise2(lipsumFilePath) {

    let upperFileName = "upper.txt"
    let upperPath = path.join(__dirname, upperFileName)
    let fileList = "filenames.txt"
    let fileListPath = path.join(__dirname, fileList)
    let splitFile = 'split.txt'
    let splitPath = path.join(__dirname, splitFile)
    let sortFile = "sort.txt"
    let sortPath = path.join(__dirname, sortFile)

    readContent(lipsumFilePath)
        .then(function (lipsumContent) {
            let uperText = lipsumContent.toUpperCase()
            //return writeContent("/a/", uperText, 'w')       //test: Invalid path upper.txt
            return writeContent(upperPath, uperText, 'w')
        })
        .then(function (message) {
            console.log(message)
            //return writeContent("/a/", "upper.txt\n", 'w')      //test: Invalid path filenames.txt
            return writeContent(fileListPath, "upper.txt\n", 'w')
        }).then(function (message) {
            console.log(message)
            return readContent(upperPath)
        }).then(function (upperContent) {
            let lower = upperContent.toLowerCase().split(".");
            let split = lower.reduce((pre, next) => {
                return (pre += next.trim() + ".\n");
            });
            //return writeContent("/a/a", split, 'w')         //test: Invalid split path
            return writeContent(splitPath, split, 'w')
        }).then(function (message) {
            console.log(message)
            return writeContent(fileListPath, "split.txt\n", 'a')
        }).then(function (message) {
            console.log(message)
            return readContent(splitPath)
        }).then(function (splitContent) {
            let sortContent = splitContent.split("\n").sort();
            let sortString = sortContent.reduce((pre, next) => {
                return (pre += next.trim() + "\n");
            });
            return writeContent(sortPath, sortString, 'w')
        }).then(function (message) {
            console.log(message)
            return writeContent(fileListPath, "sort.txt\n", 'a')
        }).then(function (message) {
            console.log(message)
            return readContent(fileListPath)
        }).then(function (filenames) {
            let arrFiles = filenames.split("\n")
            //return deleteFiles([])            //test: no files to delete
            return deleteFiles(arrFiles)
        }).then(function (status) {
            console.log(status)
        }).catch(function (err) {
            console.error(err)
        })

}

function readContent(filePath) {
    const promise = new Promise(function (resolve, rejects) {
        if (filePath === undefined || filePath === "" || typeof filePath !== 'string') {
            rejects("Invalid filepath passed")
        } else {
            fs.readFile(filePath, "utf-8", function (err, data) {
                if (err) {
                    rejects("Could not read file.", err)
                } else {
                    resolve(data)
                }
            })
        }
    })
    return promise
}

function writeContent(filepath, data, flag) {
    const promise = new Promise(function (resolve, rejects) {
        fs.writeFile(filepath, data, { flag: flag }, function (err) {
            if (err) {
                console.error("Error: could not write content.");
                rejects(err)
            } else {
                console.log(`Data written successfully on ${path.basename(filepath)}`)
                resolve(path.basename(filepath))
            }
        })
    })

    return promise
}

function deleteFiles(arrfiles) {
    const promise = new Promise(function (resolve, rejects) {
        if (arrfiles.length === 0) {
            rejects("No files to delete.")
        } else {
            let counter = 0
            function recursiveDelete(count) {
                if (count + 1 === arrfiles.length) {
                    resolve("All files deleted successfully")
                } else {
                    let filepath = path.join(__dirname, arrfiles[count])
                    fs.unlink(filepath, function (err) {
                        if (err) {
                            rejects("Error while deleting files")
                        } else {
                            console.log(`${arrfiles[count]} deleted successfully.`)
                            recursiveDelete(count + 1)
                        }
                    })
                }
            }
            recursiveDelete(counter)
        }
    })
    return promise
}


module.exports = promise2;